$(document).ready(function(){
    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 800,
        itemMargin: 5,
        minItems: 3
    });
});